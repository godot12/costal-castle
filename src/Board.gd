extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var tile_size := 200
export var board_width := 7
export var board_height := 4
var Forest_tile = preload("res://src/Tiles/Forest.tscn")
var Tile = preload("res://src/Tiles/Tile.tscn")
var Food_tile = preload("res://src/Tiles/Food.tscn")
var Rock_tile = preload("res://src/Tiles/Rock.tscn")
var Castle_tile = preload("res://src/Tiles/Castle.tscn")

var board := []
var environment_tiles := [Forest_tile, Food_tile, Rock_tile]
var _selected_tile = null setget select_tile, get_selected_tile

signal tile_selected()

func select_tile(selected_tile: Tile):
	_selected_tile = selected_tile
	emit_signal("tile_selected")

func get_selected_tile():
	return _selected_tile

func place_tile(tile : Tile, coordinates: Vector2, move=true):
	var old_tile = board[coordinates.x][coordinates.y]
	if old_tile != null:
		self.remove_child(old_tile)
		old_tile.coordinates = null
	if tile.get_parent() != self:
		self.add_child(tile)
	if tile.coordinates != null:
		board[tile.coordinates.x][tile.coordinates.y] = null
	if move:
		tile.move_to(Vector2((coordinates.x + 1) * tile_size, (coordinates.y + 1) * tile_size))
	else:
		tile.position = (Vector2((coordinates.x + 1) * tile_size, (coordinates.y + 1) * tile_size))
	board[coordinates.x][coordinates.y] = tile
	tile.coordinates = coordinates
	return old_tile
	
func exchange_tiles(coordinates_tile1 : Vector2, coordinates_tile2: Vector2):
	var tile1 = board[coordinates_tile1.x][coordinates_tile1.y]
	var tile2 = board[coordinates_tile2.x][coordinates_tile2.y]
	place_tile(tile1, coordinates_tile2)
	place_tile(tile2, coordinates_tile1)
	
func slide_column(column_id, loop=false):

	var tile = null
	if loop:
		tile = board[column_id][board_height  - 1]
		tile = place_tile(tile, Vector2(column_id, 0), true)
	else:
		tile = random_tile()
		tile = place_tile(tile, Vector2(column_id, 0), false)			
		self.remove_child(board[column_id][board_height - 1])
		board[column_id][board_height - 1].queue_free()
	for row in range(1, board_height):
		tile = place_tile(tile, Vector2(column_id, row))
	
func random_tile():
	var random_tile = environment_tiles[randi() % environment_tiles.size()]
	return random_tile.instance()
	
func create_board():
	board.resize(board_width)
	for column in range(board_width):
		board[column] = []
		board[column].resize(board_height)
	for column in range(board_width):
		for row in range(board_height):
			place_tile(random_tile(), Vector2(column, row))
			
func place_castle():
	var message_box := get_node("MessageBox")
	message_box.text = "Place your castle."
	yield(self, "tile_selected")
	place_tile(Castle_tile.instance(), _selected_tile.coordinates, false)
					
func _ready():
	randomize()
	create_board()
	place_castle()

func _on_Button_button_up():
	self.slide_column(1, true)

func _on_Board_tile_selected():
	pass # Replace with function body.
