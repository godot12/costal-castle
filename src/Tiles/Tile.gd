extends Area2D
class_name Tile
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var slide_speed = 1000
var _destination = null
var _go_to_destination = false

var coordinates = null

	
func _ready():
	pass

func move_to(destination: Vector2):
	_destination = destination
	_go_to_destination = true

func _process(delta):
	# Efficiency?
	if _go_to_destination:
		if (_destination - self.position).length() > slide_speed/10:
			self.position -= (self.position - _destination).normalized() * delta * slide_speed
		elif _destination != self.position:
			self.position = self._destination
			_go_to_destination = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Tile_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		get_parent().select_tile(self)
